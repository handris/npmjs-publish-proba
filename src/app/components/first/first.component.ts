import './first.component.scss';
import {IAugmentedJQuery, IComponentController, IComponentOptions} from 'angular';

class FirstController implements IComponentController {
    constructor(private $element: IAugmentedJQuery) { 'ngInject'; }

    $onInit() {
        console.log("I am the first component and I was created.");
    }
}

export class FirstComponent implements IComponentOptions {
    static selector = 'firstPlaceHolder';
    static template = require('./first.component.html');
    static controller = FirstController;
}