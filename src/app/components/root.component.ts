import './root.component.scss';
import { IComponentOptions } from 'angular';

export default class Root implements IComponentOptions {
    static selector = 'root';

    static template = require('./root.component.html');
}